# Maintainer: Minecrell <minecrell@minecrell.net>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname="linux-$_flavor"
pkgver=5.6_rc2
pkgrel=0
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64 armv7"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
makedepends="bison findutils flex installkernel openssl-dev perl"

# Architecture
case "$CARCH" in
	aarch64) _carch="arm64" ;;
	arm*)    _carch="arm" ;;
esac

# Source
_tag=v${pkgver//_/-}-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="dc1c6016356670dbd68db8075c742f28ea18cbde888d4076185293099ef2692b308df8465119eff90ac44fa592b931272f324a2cbb349729f3973195b96a7cd5  linux-postmarketos-qcom-msm8916-v5.6-rc2-msm8916.tar.gz
945bd74045ae26a330304bc05528e3637ac7d39f25db3bb1fe65d18d920e91bb9e633d0bc48ddc104a3b479e8ca11d4f1c528b370f6e631f7c20d9b67ab30873  config-postmarketos-qcom-msm8916.aarch64
eee448f5869142a71b9fce92cd6b02a4545f60f051c08aabc4c508296dd4e5387960904cf910c875d26218ba2c6467f9ad2dcb4973b7a82eae8126fbb15b5f7b  config-postmarketos-qcom-msm8916.armv7"
